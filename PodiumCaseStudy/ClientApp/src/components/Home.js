import React, { Component } from 'react';
import MortgageTable from './MortgageTable';
import SearchCriteria from './SearchCriteria';

export class Home extends Component {
  static displayName = Home.name;
  constructor(props) {
    super(props)
    this.state = {
      mortgages: [],
      message: ""
    }
  }

  searchMortgages(propertyValue, deposit, userID) {
    var search = {
      userId: userID,
      propertyValue: propertyValue,
      deposit: deposit
    }
    var url = "api/mortgage/mortgages/" + propertyValue + "/" + deposit + "/" + userID;
    fetch(url, {
      method: "GET"
    }).then(data => {
      return data.json();
    }).then(parsedData => {
      if(parsedData.products.length > 0){
        this.setState({message: "", mortgages: parsedData.products});
      } else {
        this.setState({message: parsedData.message, mortgages: []});
      }
    });
  }


  render() {
    return (
      <div>
        <h1>Search For Mortgages</h1>
        <SearchCriteria searchMortgages={this.searchMortgages.bind(this)} />
        { this.state.mortgages.length > 0 &&
          <MortgageTable mortgages={this.state.mortgages} />
        }
        { this.state.message != "" &&
          <h2>{this.state.message}</h2>
        }
      </div>
    );
  }
}
