import React, { Component } from 'react';

export class Applicant extends Component {
    static displayName = Applicant.name;

    constructor(props) {
        super(props);
        this.state = {
            forename: "",
            surname: "",
            dob: "",
            email: "",
            userId: ""
        };
    }

    forenameChange(event) {
        this.setState({ forename: event.target.value });
    }
    surnameChange(event) {
        this.setState({ surname: event.target.value });
    }
    dobChange(event) {
        this.setState({ dob: event.target.value });
    }
    emailChange(event) {
        this.setState({ email: event.target.value });
    }
    submitApplicant() {
        var applicant = {
            firstName: this.state.forename,
            surname: this.state.surname,
            dob: this.state.dob,
            email: this.state.email
        }
        fetch('api/mortgage/applicant', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(applicant),
        }).then(data => {
            return data.text();
        })
            .then(parsedData => {
                this.setState({ userId: parsedData });
            });
    }
    clearfields() {
        this.setState({
            forename: "",
            surname: "",
            email: "",
            dob: "",
            userId: "",
        })
    }
    render() {
        let message;
        if (this.state.userId !== "") {
            message = <div>
                <h2>Your User ID is: <b>{this.state.userId}</b> </h2>
                <p>Make a note of this</p>
            </div>;
        }
        let disabled = (this.state.dob !== "" && this.state.email !== "" && this.state.forename !== "" && this.state.surname !== "");

        return (
            <div>
                <h1>New Applicant</h1>

                <p>Enter your details</p>
                <div className="row">
                    <div className="col-md-6">
                        <label htmlFor="fornameTxt">First Name(s)</label>
                        <input id="fornameTxt" type="text" className="form-control"
                            value={this.state.forename} onChange={this.forenameChange.bind(this)} />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="surnameTxt">Surname</label>
                        <input id="surnameTxt" type="text" className="form-control"
                            value={this.state.surname} onChange={this.surnameChange.bind(this)} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <label htmlFor="emailTxt">Email</label>
                        <input id="emailTxt" type="email" className="form-control"
                            value={this.state.email} onChange={this.emailChange.bind(this)} />
                    </div>
                    <div className="col-md-6">
                        <label htmlFor="dobDate">Date Of Birth</label>
                        <input id="dobDate" type="date" className="form-control"
                            value={this.state.dob} onChange={this.dobChange.bind(this)} />
                    </div>
                </div>
                <div className="buttonRow">
                    <button className="btn btn-primary" onClick={this.submitApplicant.bind(this)} disabled={!disabled}>Submit</button>
                    <button className="btn btn-secondary" onClick={this.clearfields.bind(this)}>Clear</button>
                </div>
                {message}
            </div>
        );
    }
}
