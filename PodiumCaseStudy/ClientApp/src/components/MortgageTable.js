import React, { Component } from 'react';

class MortgageTable extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Lender</th>
            <th>Interest Rate</th>
            <th>Fixed/Variable</th>
          </tr>
        </thead>
        <tbody>
          {this.props.mortgages.map(mortgage =>
            <tr key={mortgage.Id}>
              <td>{mortgage.lender}</td>
              <td>{mortgage.interestRate}%</td>
              <td>{mortgage.mortgageType}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }
}

export default MortgageTable;