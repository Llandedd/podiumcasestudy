import React, { Component } from 'react';

class SearchCriteria extends Component {

    constructor(props) {
        super(props);
        this.state = {
            propertyValue: 0,
            deposit: 0,
            userId: ""
        };
        this.searchMortgages = this.props.searchMortgages.bind(this);
    }

    propertyValueChange(event) {
        this.setState({ propertyValue: event.target.value });
    }
    depositChange(event) {
        this.setState({ deposit: event.target.value });
    }
    userIdChange(event) {
        this.setState({ userId: event.target.value });
    }
    clearfields(event) {
        this.setState({
            userId: "",
            deposit: "",
            propertyValue: ""
        })
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-4">
                        <label htmlFor="propertyValue">Property Value (£)</label>
                        <input id="propertyValue" type="number" min="0.00" step="100" className="form-control"
                            value={this.state.propertyValue} onChange={this.propertyValueChange.bind(this)} />
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="deposit">Deposit Amount (£)</label>
                        <input id="deposit" type="number" min="0.00" step="100" className="form-control"
                            value={this.state.deposit} onChange={this.depositChange.bind(this)} />
                    </div>
                    <div className="col-md-4">
                        <label htmlFor="userId">Applicant Id</label>
                        <input id="userId" type="text" className="form-control"
                            value={this.state.userId} onChange={this.userIdChange.bind(this)} />
                    </div>
                </div>
                <div className="buttonRow">
                    <button className="btn btn-primary" 
                    onClick={() => {this.searchMortgages(this.state.propertyValue, this.state.deposit, this.state.userId)}}>Search</button>
                    <button className="btn btn-secondary" onClick={this.clearfields.bind(this)}>Clear</button>
                </div>
            </div>
        );
    }
}
export default SearchCriteria;