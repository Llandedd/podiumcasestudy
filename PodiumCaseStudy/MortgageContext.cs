﻿using Microsoft.EntityFrameworkCore;
using PodiumCaseStudy.Classes;

namespace PodiumCaseStudy
{
    public class MortgageContext : DbContext
    {
        public DbSet<Applicant> Applicants { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=mortgaging.db");
    }
}
