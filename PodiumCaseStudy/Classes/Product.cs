﻿namespace PodiumCaseStudy.Classes
{
    public class Product
    {
        public string Id { get; set; }
        public string Lender { get; set; }
        public int InterestRate { get; set; }
        public string MortgageType { get; set; }
        public int LTV { get; set; }
    }
}
