﻿namespace PodiumCaseStudy.Classes
{
    public class SearchRequest
    {
        public string UserId { get; set; }
        public int PropertyValue { get; set; }
        public int Deposit { get; set; }
    }
}
