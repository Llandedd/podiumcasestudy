﻿namespace PodiumCaseStudy.Classes
{
    public class Applicant
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
    }
}
