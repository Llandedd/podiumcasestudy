﻿namespace PodiumCaseStudy.Classes
{
    public class SearchResults
    {
        public string Message { get; set; }
        public Product[] Products { get; set; }
    }
}
