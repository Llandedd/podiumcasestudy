﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PodiumCaseStudy.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Applicants",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    DOB = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applicants", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Lender = table.Column<string>(nullable: true),
                    InterestRate = table.Column<int>(nullable: false),
                    MortgageType = table.Column<string>(nullable: true),
                    LTV = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Lender", "InterestRate", "MortgageType", "LTV" },
                values: new object[] { "1", "Bank A", 2, "Variable", 60 });
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Lender", "InterestRate", "MortgageType", "LTV" },
                values: new object[] { "2", "Bank B", 3, "Fixed", 60 });
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Lender", "InterestRate", "MortgageType", "LTV" },
                values: new object[] { "3", "Bank C", 4, "Variable", 90 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Applicants");

            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
