﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PodiumCaseStudy.Migrations
{
    public partial class RemoveUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Applicants");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Applicants",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }
    }
}
