﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PodiumCaseStudy.Classes;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PodiumCaseStudy.Controllers
{

    [Route("api/mortgage")]
    [ApiController]
    public class MortgageController : ControllerBase
    {
        // GET: api/mortgage/mortgages/propertyValue/deposit/userId
        [HttpGet]
        [Route("mortgages/{propertyValue}/{deposit}/{userId}")]
        public SearchResults GetMortgages(int propertyValue, int deposit, string userId)
        {

            var results = new SearchResults();
            var applicant = GetApplicant(userId);
            results.Message = $"No applicant with id: {userId} found";
            results.Products = new Product[0];
            if (applicant != null)
            {
                var applicantOfAge = ApplicantOfAge(applicant);
                results.Message = "Applicant not over 18";
                if (applicantOfAge)
                {
                    results.Products = GetProducts(deposit, propertyValue).ToArray();
                    if (results.Products.Count() == 0)
                    {
                        results.Message = "Deposit value too low";
                    }
                    else
                    {
                        results.Message = "Search successful";
                    }
                }
            }
            return results;
        }

        // POST api/mortgage/applicant
        [HttpPost]
        [Route("applicant")]
        public string PostApplicant([FromBody] Applicant applicant)
        {
            applicant.Id = Guid.NewGuid().ToString();
            AddApplicant(applicant);
            return applicant.Id;
        }

        private bool ApplicantOfAge(Applicant applicant)
        {
            var dob = DateTime.Parse(applicant.DOB);
            var dateToCompare = DateTime.Now.AddYears(-18);
            return dateToCompare.CompareTo(dob) >= 0;
        }

        private List<Product> GetProducts(int deposit, int value)
        {
            var results = new List<Product>();
            var mortgageAmount = Convert.ToDouble(value - deposit);
            var ltv = (int)(mortgageAmount / Convert.ToDouble((value)) * 100);
            using var db = new MortgageContext();
            return db.Products.Where(p => p.LTV > ltv).ToList();
        }

        private void AddApplicant(Applicant applicant)
        {
            using var db = new MortgageContext();
            db.Applicants.Add(new Applicant
            {
                DOB = applicant.DOB,
                Email = applicant.Email,
                FirstName = applicant.FirstName,
                Id = applicant.Id,
                Surname = applicant.Surname
            });
            db.SaveChanges();
        }
        private Applicant? GetApplicant(string id)
        {
            using var db = new MortgageContext();
            return db.Applicants
                .FirstOrDefault(a => a.Id == id);
        }
    }

}
